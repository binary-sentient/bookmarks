from django import forms
from django.contrib.auth.models import User

from .models import Profile

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    # This provides custom validation for the password2 field (ensures that
    # both passwords match)

    # By default, the general clean() function only validates the requirements
    # set by the model definition. This validation is in addition to that
    # basic validation.

    def clean_password2(self):
        cd = self.cleaned_data

        if cd.get('password') != cd.get('password2'):
            raise forms.ValidationError('Passwords don\'t match.')
        
        return cd.get('password2')

class UserEditForm(forms.ModelForm):
    """ Allows user to edit name and email, which are parts of the base Django user model
    """
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class ProfileEditForm(forms.ModelForm):
    """ Allows user to edit their dob and photo, which are extensions to the Django user model
    """
    class Meta:
        model = Profile
        fields = ('date_of_birth', 'photo')